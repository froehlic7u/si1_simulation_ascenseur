import java.util.ArrayList;

public class Etage extends Constantes {

	private int numero; // de l'Etage pour l'usager

	private Immeuble immeuble; // de l'Etage

	private LoiDePoisson poissonFrequenceArrivee; // dans l'Etage

	private ArrayList<Passager> passagers = new ArrayList<Passager>();

	public Etage(int n, int fa, Immeuble im) {
		numero = n;
		immeuble = im;
		int germe = n << 2;
		if (germe <= 0) {
			germe = -germe + 1;
		}
		poissonFrequenceArrivee = new LoiDePoisson(germe, fa);
	}

	public void affiche() {
		if (numero() >= 0) {
			System.out.print(' ');
		}
		System.out.print(numero());
		if (this == immeuble.cabine.etage) {
			System.out.print(" C ");
			if (immeuble.cabine.porteOuverte) {
				System.out.print("[  ]: ");
			} else {
				System.out.print(" [] : ");
			}
		} else {
			System.out.print("   ");
			System.out.print(" [] : ");
		}
		int i = 0;
		boolean stop = passagers.size() == 0;
		while (!stop) {
			if (i >= passagers.size()) {
				stop = true;
			} else if (i > 6) {
				stop = true;
				System.out.print("...(");
				System.out.print(passagers.size());
				System.out.print(')');
			} else {
				passagers.get(i).affiche();
				i++;
				if (i < passagers.size()) {
					System.out.print(", ");
				}
			}
		}
		System.out.print('\n');
	}

	public int numero() {
		return this.numero;
	}

	public void ajouter(Passager passager) {
		assert passager != null;
		passagers.add(passager);
	}

	public long arriveeSuivant() {
		return poissonFrequenceArrivee.suivant();
	}
	
	public int passagers(){
		return passagers.size();
	}
	
	public Passager recupererPassager(int i){
		return passagers.get(i);
	}
	
	public void supprimerPassager(int i){
		passagers.remove(i);
	}
	
	public Passager recupererPremierPassager(){
		return passagers.get(0);
	}
	
	public void supprimerPremierPassager(){
		passagers.remove(0);
	}
	
	public boolean arretEtage(){
		if(passagers.size() != 0){
			int i = 0;
			if(immeuble.cabine.donnerPlaces() == Constantes.nombreDePlacesDansLaCabine)
				return true;
			while(i < passagers.size()){
				if(passagers.get(i).sens() == immeuble.cabine.donnerDirection())
					return true;
				else
					i++;
			}
			return false;
		}
		return false;
	}
	
	public void incrementerCumulTransport(long t) {
		immeuble.cumulDesTempsDeTransport += t;
		immeuble.nombreTotalDesPassagersSortis ++;
	}

}
