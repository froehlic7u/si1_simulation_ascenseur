public class EvenementPassageCabinePalier extends Evenement {

	private Etage etage;

	public EvenementPassageCabinePalier(long d, Etage e) {
		super(d);
		etage = e;
	}

	public void afficheDetails(Immeuble immeuble) {
		System.out.print("PCP ");
		System.out.print(etage.numero());
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		Cabine cabine = immeuble.cabine;
		assert !cabine.porteOuverte;
		assert etage.numero() != cabine.etage.numero();
		cabine.etage = etage;
		
		if(immeuble.cabine.verifierDestination() || etage.arretEtage()){
			echeancier.ajouter(new EvenementOuverturePorteCabine(date+Constantes.tempsPourOuvrirOuFermerLesPortes));
		}
		else{
			switch(cabine.status()){
			case 'v':
				echeancier.ajouter(new EvenementPassageCabinePalier(date+Constantes.tempsPourBougerLaCabineDUnEtage, immeuble.etage(etage.numero()-1)));
				break;
			case '^':
				echeancier.ajouter(new EvenementPassageCabinePalier(date+Constantes.tempsPourBougerLaCabineDUnEtage, immeuble.etage(etage.numero()+1)));
				break;
			default:
				break;
			}
		}
		
		// ...
		// ...
		// ...
		// ...
		assert !cabine.porteOuverte;
	}

}
