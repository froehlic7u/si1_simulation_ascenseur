public class EvenementOuverturePorteCabine extends Evenement {

	public EvenementOuverturePorteCabine(long d) {
		super(d);
	}

	public void afficheDetails(Immeuble immeuble) {
		System.out.print("OPC");
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		Cabine cabine = immeuble.cabine;
		Etage etage = cabine.etage;

		assert cabine.status() != '-';
		assert !cabine.porteOuverte;
		cabine.porteOuverte = true;
		
		long temps=date+Constantes.tempsPourOuvrirOuFermerLesPortes;
		int places = cabine.donnerPlaces();
		boolean continuer = true;
		temps += immeuble.cabine.faireDescendrePassagers(date);
		int i = 0;
		while(places > 0 && continuer){
			if(i < etage.passagers()){
				Passager p = etage.recupererPassager(i);
				if(p.sens() == immeuble.cabine.status()){
					cabine.faireMonterPassager(p);
					etage.supprimerPassager(i);
					temps+=tempsPourEntrerOuSortirDeLaCabine;
					places--;
				}else
					i++;
			}else
				continuer=false;
		}
		if(places == Constantes.nombreDePlacesDansLaCabine){
			while (places > 0 && !continuer) {
				if(etage.passagers() > 0){
					Passager p = etage.recupererPremierPassager();
					cabine.faireMonterPassager(p);
					etage.supprimerPremierPassager();
					temps+=tempsPourEntrerOuSortirDeLaCabine;
					places--;
					i++;
				}else
					continuer=true;
			}
		}
		if(cabine.donnerPlaces() != Constantes.nombreDePlacesDansLaCabine)
			cabine.changerStatus(cabine.donnerDirection());
		echeancier.ajouter(new EvenementFermeturePorteCabine(temps));
		
		
		
		
		//notYetImplemented();
		// ...
		// ...
		// ...
		// ...

		assert cabine.porteOuverte;
	}

}
