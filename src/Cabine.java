public class Cabine extends Constantes {

	public Etage etage; // actuel

	public boolean porteOuverte;

	private char status; // '-' ou 'v' ou '^'

	private Passager[] tableauPassager;

	public Cabine(Etage e) {
		etage = e;
		tableauPassager = new Passager[nombreDePlacesDansLaCabine];
		porteOuverte = false;
		status = '-';
	}

	public void afficheLaSituation() {
		System.out.print("Contenu de la cabine: ");
		for (int i = tableauPassager.length - 1; i >= 0 ; i--) {
			Passager p = tableauPassager[i];
			if (p != null) {
				p.affiche();
				System.out.print(' ');
			}
		}
		assert (status == 'v') || (status == '^') || (status == '-');
		System.out.println("\nStatus de la cabine: " + status);
	}

	public boolean transporte(Passager p) {
		// Pour savoir si le passager p est dans la Cabine.
		// Lent: Pour utilisation dans les asserts.
		assert p != null;
		for (int i = tableauPassager.length - 1 ; i >= 0  ; i --) {
			if (tableauPassager[i] == p) {
				return true;
			}
		}
		return false;
	}

	public char status() {
		assert (status == 'v') || (status == '^') || (status == '-');
		return status;
	}

	public void changerStatus(char s){
		assert (s == 'v') || (s == '^') || (s == '-');
		status = s;
	}
	
	public int donnerPlaces(){
		int c = 0;
		int i = 0;
		while(i<tableauPassager.length){
			if(tableauPassager[i] == null) c+=1;
			i++;
		}
		
		return c;
	}

	public boolean faireMonterPassager(Passager p) { 
		assert p != null;
		assert ! transporte(p);
		
		tableauPassager[nombreDePlacesDansLaCabine-donnerPlaces()]=p;		
		
		//notYetImplemented();
		return false;
	}
	
	public boolean verifierDestination(){
		int i = 0;
		while(i<tableauPassager.length){
			if(tableauPassager[i] != null && tableauPassager[i].etageDestination()==etage){
				return true;
			}
			i++;
		}
		return false;
	}
	
	public long faireDescendrePassagers(long d){
		int i = 0;
		long temps = 0;
		while(i<tableauPassager.length) {
			if(tableauPassager[i] != null && tableauPassager[i].etageDestination()==etage){
				int j = i;
				etage.incrementerCumulTransport(d-tableauPassager[i].dateDepart());
				while(j<tableauPassager.length-1) {
					tableauPassager[j] = tableauPassager[j+1];
					j++;
				}
				tableauPassager[j] = null;
				temps += Constantes.tempsPourEntrerOuSortirDeLaCabine;
			}else
				i++;
		}
		return temps;
	}
	
	public char donnerDirection(){
		assert tableauPassager[0]!=null;
		return tableauPassager[0].sens();
	}

}

