public class EvenementFermeturePorteCabine extends Evenement {

	public EvenementFermeturePorteCabine(long d) {
		super(d);
	}

	public void afficheDetails(Immeuble immeuble) {
		System.out.print("FPC");
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		Cabine cabine = immeuble.cabine;
		assert cabine.porteOuverte;
		cabine.porteOuverte = false;
		
		immeuble.calculerStatusCabine();
			
		
		switch (cabine.status()) {
		case '^':
			echeancier.ajouter(new EvenementPassageCabinePalier(date + tempsPourBougerLaCabineDUnEtage, immeuble.etage(cabine.etage.numero()+1)));
			break;
		case 'v':
			echeancier.ajouter(new EvenementPassageCabinePalier(date + tempsPourBougerLaCabineDUnEtage, immeuble.etage(cabine.etage.numero()-1)));
			break;
		default:
			assert cabine.status() == '-';
//			notYetImplemented();
		}
		//notYetImplemented();

		// ...
		// ...
		// ...
		assert ! cabine.porteOuverte;
	}

	public void setDate(long d){
		this.date = d;
	}

}
