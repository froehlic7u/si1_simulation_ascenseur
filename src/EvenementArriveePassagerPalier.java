public class EvenementArriveePassagerPalier extends Evenement {

	private Etage etageDeDepart;

	public EvenementArriveePassagerPalier(long d, Etage edd) {
		super(d);
		etageDeDepart = edd;
	}

	public void afficheDetails(Immeuble immeuble) {
		System.out.print("APP ");
		System.out.print(etageDeDepart.numero());
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		assert etageDeDepart != null;
		assert immeuble.etage(etageDeDepart.numero()) == etageDeDepart;
		echeancier.ajouter(new EvenementArriveePassagerPalier(date+etageDeDepart.arriveeSuivant(), etageDeDepart));
		Passager p = new Passager (date, etageDeDepart, immeuble);
		etageDeDepart.ajouter(p);
		int nombreEtages = this.etageDeDepart.numero() - immeuble.cabine.etage.numero();
		if(immeuble.cabine.status() == '-'){
			if(nombreEtages > 0){
				echeancier.ajouter(new EvenementPassageCabinePalier(date+Constantes.tempsPourBougerLaCabineDUnEtage, immeuble.etage(immeuble.cabine.etage.numero()+1)));
				immeuble.cabine.changerStatus('^');
			}
			else if(nombreEtages < 0){
				echeancier.ajouter(new EvenementPassageCabinePalier(date+Constantes.tempsPourBougerLaCabineDUnEtage, immeuble.etage(immeuble.cabine.etage.numero()-1)));
				immeuble.cabine.changerStatus('v');
			}
		}
//		else {
//			if(nombreEtages > 0)
//				immeuble.cabine.changerStatus('^');
//			else
//				immeuble.cabine.changerStatus('v');
//		}



		//notYetImplemented();
	}
}
